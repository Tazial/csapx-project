Extra Credit Overview
=====================

 * NetworkClient, PlaceGUI, and ProtectionBot all implement the ability to
   protect one or multiple tiles in a pattern.  This can be saved and loaded
   from disk, allowing one to resume protecting an area even if the client
   stops briefly.  Information on the file format can be found in the javadocs
   for `place.network.NetworkClient.saveProtections(OutputStream)`, which is
   where the saving and loading occurs.  Also look at the `sendSingle()` and
   `sendLoop()` methods of the `NetworkClient` if you're interested in how this
   is executed.

 * The server has been hardened against thread bombing, and the insertion of
   bad data.  It is also resistant against attacks based on the tendancy of
   ObjectInputStreams to hang during their construction if the underlying input
   stream blocks.  Look at the `place.server.PlaceServer.run()` method for
   in-depth documentation on how this is accomplished.

 * Every tile received by the server can optionally be logged to a file.  This
   file uses a binary format described in the `LogWriter` class, and can be
   used to allow a server to resume from where it left off without reseting the
   board.  It also could theoretically be analyzed for statistical purposes
   very easily, since the `LogIterator` class can be used with java Streams or
   other `Iterator` based tools to do statistical analysis.

 * The GUI client can zoom and pan using both the mouse and keyboard.  This
   allows it to view boards of any size.  See Ari's documentation on this and
   several other useful features of the client by clicking the "Instructions"
   button in the GUI.

 * An alternative server implementation has been written that plays Snake.
   Checkout the snake branch and run the server to try it.  The SnakeServer is
   compatible with any client that can connect to the regular PlaceServer.
 
 * A bot that cycles through all the tiles on the board one by one, a bot that
   does the same thing in a snaking pattern, one that protects a specific
   design, and one that just places random tiles everywhere.
