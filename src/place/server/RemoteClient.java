package place.server;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import place.PlaceBoard;
import place.PlaceTile;
import place.network.PlaceRequest;

/**
 * A class to abstract communications with remote servers using RIT's place
 * protocol.  Extends thread to allow running listening in another thread.
 * Abstracts all read/write communications with the clients into event
 * listeners and methods.
 *
 * @author Taizo Simpson <tts1848@cs.rit.edu>
 */
public class RemoteClient extends Thread implements Closeable {

    private Socket conn;
    private ObjectInputStream recv;
    private ObjectOutputStream send;
    private String username;
    private PlaceBoard board;
    private Instant canPlaceAfter = Instant.EPOCH;

    private Consumer<String> onLoginAttempt;
    private Consumer<PlaceTile> onTileReceived;
    private List<Runnable> onDisconnect = new ArrayList<>();

    /** The timeout to use for read operations for clients that haven't
     * finished to login handshake.*/
    private static int TIMEOUT = 2000;

    /** Creates a new remote client for the given socket.  Will establish
     * object streams with the client over the socket, and may hang if the user
     * doesn't reciprocate the streams.
     *
     * @param socket The socket to use to communicate with the client
     * @throws IOException A network error occured while connecting to the client
     * @throws SocketTimeoutException The client timed out while setting up the object
     *                                streams.  In this case, the socket will be
     *                         closed, and a message will be printed to stdout.
     */
    public RemoteClient(Socket socket, PlaceBoard board) throws IOException, SocketTimeoutException {
        this.board = board;
        this.conn = socket;
        this.conn.setSoTimeout(TIMEOUT);
        try {
            this.send = new ObjectOutputStream(socket.getOutputStream());
            this.recv = new ObjectInputStream(socket.getInputStream());
        } catch(SocketTimeoutException e) { // Timeout
            socket.close();
            System.out.printf(
                "Info: Client with ip address %s failed to complete ObjectStream handshake in time%n",
                socket.getInetAddress().getHostName()
            );
            throw e;
        }

        this.onLoginAttempt = (username) -> {
            System.out.printf("Info: Login attempt from %s with username %s.%n", socket.getInetAddress().getHostName(), username);
        };
        this.onTileReceived = (tile) -> {
            System.out.printf("Debug: Tile recieved from client %s: %s%n", this.getIdentifier(), tile);
        };
        this.onDisconnect.add(() -> {
            System.out.printf("Info: Client %s disconnected.%n", this.getIdentifier());
        });
    }

    /**
     * Runs the clients main loop.  This starts listening for messages from the
     * client, and calling appropriate event listeners where neccessary.  May
     * also send errors to the client if an invalid request is sent.  Can be
     * started using {@link start()} to run in a new thread.
     */
    @Override
    public void run() {
        while(true) {
            try {
                PlaceRequest<?> req = (PlaceRequest<?>) this.recv.readUnshared();
                synchronized(this) {
                    switch(req.getType()) {
                        case LOGIN:
                            if(this.username == null) {
                                this.onLoginAttempt.accept((String) req.getData());
                            } else {
                                this.sendError("Session already logged in.  Please do not request a username more than once.");
                            }
                            break;
                        case CHANGE_TILE:
                            // User must be logged in
                            if(this.username == null) {
                                this.sendError("Please complete the log in process before attempting to change to board.");
                                continue;
                            }

                            // Cannot place until after the given time
                            Instant now = Instant.now();
                            if(now.isBefore(this.canPlaceAfter)) {
                                this.sendError("Cannot place another tile until after " + this.canPlaceAfter);
                                continue;
                            }

                            // Cannot place outside of bounds
                            PlaceTile tile = (PlaceTile) req.getData();
                            int size = this.board.getBoard().length;
                            if(
                                tile.getRow() < 0 ||
                                tile.getRow() >= size ||
                                tile.getCol() < 0 ||
                                tile.getCol() >= size
                            ) {
                                this.sendError("Tile must be within the bounds of the board");
                                continue;
                            }

                            synchronized(this.board) {
                                // Cannot overwrite a tile with a tile of the same color
                                if(this.board.getTile(tile.getRow(), tile.getCol()).getColor() == tile.getColor()) {
                                    this.sendError("Tile sent must differ in color from the existing tile.");
                                    continue;
                                }

                                tile = new PlaceTile(
                                    tile.getRow(),
                                    tile.getCol(),
                                    this.username,
                                    tile.getColor(),
                                    now.toEpochMilli()
                                );
                                this.board.setTile(tile);
                            }

                            this.canPlaceAfter = now.plusMillis(500);
                            this.onTileReceived.accept(tile);
                    }
                }
            } catch(SocketTimeoutException e) { // Thrown if the connection times out.
                try {
                    this.conn.close();
                } catch(IOException e2) { }
                // Because we remove the timeout after the login is completed,
                // we know that this client failed to log in fast enough.
                System.out.printf("Info: Client %s failed to submit login in time%n", this.getIdentifier());
            } catch(IOException e) {
                try {
                    this.conn.close();
                } catch(IOException e2) { }
                this.onDisconnect.forEach(Runnable::run);
                break;
            } catch(ClassNotFoundException e) {
                System.out.printf("Warning: Recieved invalid data from client %s.%n", this.getIdentifier());
            }
        }
    }

    /**
     * Notify this client that a tile has been changed.  Corresponds to the
     * TILE_CHANGED message.  Note that this does not verify the validity of
     * the changed tile.
     *
     * @param tile The tile that has changed
     */
    public synchronized void sendTile(PlaceTile tile) {
        try {
            this.send.writeUnshared(
                new PlaceRequest<>(PlaceRequest.RequestType.TILE_CHANGED, tile)
            );
        } catch(IOException e) {} // Will be handled by the main thread
    }

    /**
     * Notify the client that an error has occured.
     *
     * @param msg The message to be sent along with the error
     */
    public synchronized void sendError(String msg) {
        try {
            this.send.writeUnshared(
                new PlaceRequest<>(PlaceRequest.RequestType.ERROR, msg)
            );
        } catch(IOException e) {} // Will be handled by the main thread
    }

    /**
     * Assign a username to this client.  If a client hasn't yet been assigned
     * a username, assign one to them and notify them via the LOGIN_SUCCESS
     * message.
     *
     * @pre The user has not yet been assigned a username
     * @param username The username to assign to the user
     * @throws IllegalStateException The client had already been assigned a
     *                               username.
     */
    public synchronized void assignUsername(String username) throws IllegalStateException {
        if(this.username == null) {
            this.username = username;
            try {
                this.send.writeUnshared(
                    new PlaceRequest<>(PlaceRequest.RequestType.LOGIN_SUCCESS, username)
                );
                this.send.writeUnshared(
                    new PlaceRequest<>(PlaceRequest.RequestType.BOARD, this.board)
                );
                this.conn.setSoTimeout(0); // Remove the timeout now that the client is logged in.
            } catch(IOException e) {} // Will be handled by the main thread
        } else {
            throw new IllegalStateException("Cannot assign username after username has already been assigned");
        }
    }

    /**
     * Register an event listener for when a client attempts to connect.  This
     * is called whenever a client that hasn't yet been assigned a username
     * sends a LOGIN message.  It is expected that exactly one listener then
     * assign a username to the client using the {@link assignUsername(String)}
     * method.
     *
     * @param onLoginAttempt A consumer to be called when a client attempts to
     *                       login.  Accepts the client's proposed username.
     */
    public synchronized void registerOnLoginAttempt(Consumer<String> onLoginAttempt) {
        this.onLoginAttempt = this.onLoginAttempt.andThen(onLoginAttempt);
    }

    /**
     * Register an event listener for when a client sends a valid tile change.
     * This consumer is called with the tile a client would like to change
     * whenever a valid request to change a tile is received from the client.
     * In order to be considered valid, the tile must differ in color from the
     * current tile at the given co-ordinates, must be within the bounds of the
     * board, and must have been sent more than 500ms after the last valid tile
     * was sent.  The username and timestamp on the received tile are updated
     * and guaranteed to be accurate.
     *
     * @param onTileReceived A consumer accepting a PlaceTile to be called on
     *                       the event.
     */
    public synchronized void registerOnTileReceived(Consumer<PlaceTile> onTileReceived) {
        this.onTileReceived = this.onTileReceived.andThen(onTileReceived);
    }

    /**
     * Register an event listener for when a client disconnects from the
     * server.  The provided runnable will be executed no more than once, when
     * the user loses connection to the server for any reason.  It will not be
     * called if the client has disconnected before the listener is registered,
     * nor if the client's thread isn't running or stops.
     *
     * @param onDisconnect The function to run when the user disconnects.
     */
    public synchronized void registerOnDisconnect(Runnable onDisconnect) {
        this.onDisconnect.add(onDisconnect);
    }

    /**
     * Gets an human readable way to identify this client by one of its
     * properties.  If the client has a username assigned to it, the returned
     * value will resemble "with username xxxx", and otherwise, the value will
     * resemble "with ip address x.x.x.x".  This is meant to be used to refer to the
     * client like "Client username xxxx just did something!"
     *
     * @returns A human-readable string to refer to this client
     */
    public String getIdentifier() {
        if(this.username == null) {
            return "with ip address " + this.conn.getInetAddress().getHostName();
        } else {
            return "with username " + this.username;
        }
    }

    /**
     * Returns the username assigned to this client, or null if one hasn't yet
     * been assigned.
     *
     * @return The client's assigned username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Get the socket being used to communicate with the client.
     *
     * @return The connection to the client
     */
    public Socket getSocket() {
        return this.conn;
    }

    public void close() throws IOException {
        this.conn.close();
    }
}
