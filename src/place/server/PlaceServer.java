package place.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

import place.PlaceBoard;
import place.PlaceTile;
import place.logging.LogIterator;
import place.logging.LogWriter;

/**
 * A server compliant with the RIT Place protocol.
 *
 * @author Taizo Simpson <tts1848@cs.rit.edu>
 * @author Ari Wisenburg <aew1756@rit.edu>
 */
public class PlaceServer extends Thread {

    private int port;
    private PlaceBoard board;
    private ServerSocket mainSock;
    private Set<InetAddress> pendingClients = new HashSet<>();
    private Map<InetAddress, RemoteClient> clients = new HashMap<>();
    private LogWriter log;

    /**
     * Construct a new PlaceServer around the given address.  Changes will not
     * be logged.
     * @param port The port to host the server on
     * @param size The length/height of the board to host
     */
    public PlaceServer(int port, int size) {
        this(port, size, (LogWriter) null);
    }

    /**
     * Construct a new PlaceServer around the given address, with changes
     * logged to the given file compressed with GZip.
     * @param port The port to host the server on
     * @param size The length/height of the board to host
     * @param logFile The file to which to log changes to.  Not null.
     * @throws IOException An I/O error occured while trying to setup the log.
     */
    public PlaceServer(int port, int size, File logFile) throws IOException{
        this(port, size, new LogWriter(new FileOutputStream(logFile, true)));
    }

    /**
     * Construct a new PlaceServer around the given address, with changes
     * logged in the given file, if not null.
     * @param port The port to host the server on
     * @param size The length/height of the board to host
     * @param logFile The file to which to log changes to.  Setting this to
     * null disables logging.
     */
    public PlaceServer(int port, int size, LogWriter log) {
        this.port = port;
        this.board = new PlaceBoard(size);
        this.log = log;
    }

    /**
     * Start accepting new connections from clients.  This will begin listening
     * for clients on the port that the server was constructed with, and
     * spawning a new thread for each client that connects.  The clients
     * connections are automatically passed to a {@link RemoteClient}, which
     * handles handshaking with the client.  Precautions are taken against
     * thread bombing, as well as creating timeouts by exploiting hanging
     * Object Streams.  Only one connection is allowed per ip address to this
     * end, which may cause problems with clients behind NAT.  Clients that
     * fail to handshake, or that time out, will be disconnected.
     *
     * This method will log incoming connections and occasionally unusual
     * disconnects.
     */
    @Override
    public void run() {
        try {
            this.mainSock = new ServerSocket(port);
            while(true) {
                Socket conn = this.mainSock.accept();

                /* Setting up the client could take up to two seconds, so it's
                 * best to do it in a throwaway thread.  Note that this can't
                 * be bombed easily because of a check early on that will close
                 * the connection and the thread if another connection is found
                 * to be in progress.  See the big comment after this one.
                 */
                new Thread(() -> {

                    /* The pending clients set contains all of the clients in
                     * "limbo", i.e. the the period after the socket connection has
                     * been created, but before the RemoteClient has finished
                     * constructing.  A malicious client can be designed to keep
                     * RemoteClient in its constructor for up to two seconds before
                     * the timeout kicks in and closes the connection.  If we
                     * continue accepting new connections during this period, the
                     * malicious party could circumvent the "one connection per ip"
                     * rule by creating many threads that last two seconds each.
                     * To prevent this, we track ips who have a connection in limbo
                     * and disallow connections from that ip while they remain in
                     * limbo.  No client, malicious or otherwise, can remain in
                     * limbo for more than two seconds, so this won't hurt regular
                     * users with slow internets.
                     */
                    InetAddress address = conn.getInetAddress();
                    synchronized(this.pendingClients) {
                        if(this.pendingClients.contains(address)) {
                            System.out.printf("Info: Rejecting connection " +
                                    "from %s due to pre-existing pending " +
                                    "connection.%n", address.getHostName());
                            try {
                                conn.close();
                            } catch(IOException e) {}
                            return;
                        }
                        else {
                            this.pendingClients.add(address);
                        }
                    }

                    // Construct the remote client.  This is the part that could take up to two seconds
                    RemoteClient client;
                    try {
                        client = new RemoteClient(conn, this.board);

                        // Replace any previous connection from this address,
                        // closing the old one if necessary.
                        RemoteClient oldConnection = this.clients.put(address, client);
                        if(oldConnection != null) {
                            oldConnection.close();
                        }
                    } catch (SocketTimeoutException e) { // Thrown if setting up the connection times out.
                        return;
                    } catch (java.io.StreamCorruptedException e) {
                        System.out.println("Info: Got malformed headers from ip " + address.getHostName());
                        return;
                    } catch(IOException e) {
                        System.out.println("Unexpected error: " + e);
                        return;
                    } finally {

                        // Remove client from limbo
                        synchronized(this.pendingClients) {
                            this.pendingClients.remove(address);
                        }
                    }

                    //checks how many people are on the server and
                    // disconnects the client if too many people are already on.
                    synchronized(this) {
                        System.out.println(clients.size());
                        if (clients.size() > (board.DIM/4)) {
                            System.out.printf("Info: Rejecting connection " +
                                    "from %s because too many people are on " +
                                    "the server at the moment. Please try " +
                                    "again later.%n", address.getHostName());
                            client.sendError("Too many people were connected to " +
                                    "the server at once. Please try connecting " +
                                    "again later.");
                            this.removeClient(client);
                            try {
                                conn.close();
                            } catch (IOException e) {
                            }
                            return;
                        }
                    }


                    // Setup event listeners
                    client.registerOnLoginAttempt(uname -> this.loginClient(client, uname));
                    client.registerOnTileReceived(this::sendToAll);
                    client.registerOnDisconnect(() -> this.removeClient(client));

                    // Finalize
                    System.out.printf("Info: Client %s connected.%n", client.getIdentifier());
                    client.start();

                }).start();
            }
        } catch (SecurityException e) {
            System.err.println("Failed to bind to port: permision denied");
            System.err.println(e);
        } catch (IOException e) {
            // All IOExceptions should be caught somewhere in the body.  This
            // should never run, but if it does, we should have information
            // about where.
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets the board currently in use by the server.  Should NEVER be called
     * while the server is running
     *
     * @param board The board which should replace the current board.
     */
    public void setBoard(PlaceBoard board) {
        if(this.isAlive()) {
            throw new IllegalStateException("setBoard should never be used while a PlaceServer is running");
        }
        this.board = board;
    }

    /**
     * Start up a server from the command line.  This takes two required
     * positional command line arguments, being the port that the server will
     * listen on, and the size of the board to be used, as well as a single
     * optional positional argument, being the path to the log file.  If either
     * argument is missing or invalid, the server will not run, and usage
     * information is printed to the command line.
     *
     * @param args Any arguments passed from the command line
     */
    public static void main(String[] args) {
        if(args.length <2) {
            usage("Not enough arguments");
        } else {
            try {
                if(args.length >= 3) {
                    File logFile = new File(args[2]);
                    logFile.createNewFile();

                    PlaceBoard board = new PlaceBoard(Integer.parseInt(args[1]));
                    new LogIterator(new FileInputStream(logFile)).forEachRemaining(board::setTile);

                    PlaceServer server = new PlaceServer(
                        Integer.parseInt(args[0]),
                        0,
                        logFile
                    );
                    server.setBoard(board);
                    server.start();
                } else {
                    new PlaceServer(
                        Integer.parseInt(args[0]),
                        Integer.parseInt(args[1])
                    ).start();
                }
            } catch (NumberFormatException e) {
                usage("Port and size must be integers.");
            } catch (IOException e) {
                System.err.println("There was a problem accessing the log file: " + e);
            }
        }
    }

    /**
     * Prints usage information to stdout, and then exit.
     * @param error The error message to be printed along with the usage information.  Should not be null.
     */
    public static void usage(String error) {
        System.out.println("Usage: java PlaceServer <port> <size> [log file]");
        System.out.println(error);
        System.exit(-1);
    }

    // Helper method that should be called whenever a client requests a
    // username.  Finds a unique username similar to the requested one, and
    // assigns it to the client.
    private void loginClient(RemoteClient client, String requestedUsername) {
        int suffix = 0;
        String username = requestedUsername;
        HashSet<String> usernames;
        synchronized(this) {
            usernames = this.clients.values()
                                    .stream()
                                    .map(RemoteClient::getUsername)
                                    .collect(Collectors.toCollection(HashSet::new));
        }
        while(usernames.contains(username)) {
            username = requestedUsername + ++suffix;
        }
        client.assignUsername(username);
    }

    // Broadcast a tile change to all connected clients
    // Also logs the tile, if logging is enabled
    private void sendToAll(PlaceTile tile) {
        this.clients.values()
               .stream()
               .parallel()
               .forEach(c -> c.sendTile(tile));
        synchronized(this) {
            try {
                if(this.log != null)
                    this.log.writeEntry(tile);
            } catch(IOException e) {
                System.err.println("Warning: IOException while writing to log.  Disabling logging");
                this.log = null;
            }
        }
    }

    //Remove a client from the list of connected clients
    private void removeClient(RemoteClient client) {
        this.clients.remove(client.getSocket().getInetAddress(), client);
    }
}
