package place.client.ptui;

import place.PlaceColor;
import place.PlaceBoard;
import place.PlaceTile;
import place.network.NetworkClient;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

/**
 * The plain text UI for Place
 *
 * @author Vivian Nowka-Keane <vdn1708@rit.edu>
 */
public class PlacePTUI extends ConsoleApplication implements Observer{

    /**
     * Server
     */
    private NetworkClient conn;

    /**
     * What to read to see what user types
     */
    private Scanner userIn;

    /**
     * Where to send text that the user can see
     */
    private PrintWriter userOut;

    /**
     * ID to send with tile changes
     */
    private String username;

    /**
     * Run loop while user is connected to the server
     */
    private boolean userLoggedIn;


    /**
     *  Create the server connection and log the user in, and set up the server
     *  event listener.
     */
    @Override
    public void init(){
        try {
            List<String> args = super.getArguments();

            // Parse arguments
            String hostname = args.get(0);
            int port = Integer.parseInt(args.get(1));
            this.username = args.get(2);

            // Establish connection and log in
            this.conn = new NetworkClient(hostname, port, this.username);
            this.userLoggedIn = true;

            // Notify thread when tile had been changed
            this.conn.registerOnChange((tile) -> {
                synchronized (this) {
                    notifyAll();
                }
            });

            System.out.println("Successfully connected to server");

        } catch (InterruptedException e) {
            System.err.println("Warning: Interrupted during login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method continues running until the game is over.
     * This method waits for a notification from {@link #endGame()},
     * called from this class' {@link #runBoard()} ()} method
     *
     * @param userIn what to read to see what user types
     * @param userOut where to send messages so user can see them
     */
    public synchronized void go(Scanner userIn, PrintWriter userOut ) {

        this.userIn = userIn;
        this.userOut = userOut;

        // Manually force a display of all board state, since it's too late
        // to trigger update().
        this.userOut.println(this.conn.getBoard());
        this.runBoard();

    }

    /**
     * GUI is closing, so close the network connection. Server will
     * get the message.
     */
    @Override
    public void stop() {
        this.userIn.close();
        this.userOut.close();
        this.conn.close();
        userLoggedIn = false;
    }

    private synchronized void endGame() {
        this.notify();
        this.stop();
    }

    /**
     * Traverse through PlaceColor enum values and compare each 'number'
     * variable to the given int.
     *
     * @param colorNum number value that corresponds to a color
     * @return PlaceColor value based on enum
     */
    private PlaceColor findColor(int colorNum){
        for(PlaceColor c : PlaceColor.values()) {
            if (c.getNumber() == colorNum){
                return c;
            }
        }
        this.userOut.println("Color number out of range");
        return null;
    }

    /**
     * If the PlaceTile is valid, send it to the server and wait for it to be pulled
     * from the queue before printing the updated board to the user.
     *
     * @param tile data to send to server
     */
    public void update(PlaceTile tile) {

        int row = tile.getRow();
        int col = tile.getCol();
        PlaceColor color = tile.getColor();

        if (this.conn.getBoard().isValid(tile)) {
            this.conn.changeTile(row, col, color);
        } else {
            this.userOut.println("Invalid tile");
        }

        // wait for tile to be changed and thread to be notified
        try {
            synchronized (this) {
                wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.userOut.println(this.conn.getBoard());
    }

    /**
     * While the user is logged in, accept user input and call
     * {@link #update(PlaceTile)} ()} ()}
     * End the game if the user isn't logged in.
     */
    public void runBoard(){
        while(userLoggedIn){
            this.userOut.print("type move as: row col color");
            this.userOut.flush();
            int row = this.userIn.nextInt();
            if(row == -1){
                this.endGame();
                break;
            }
            int col = this.userIn.nextInt();
            int colorNum = this.userIn.nextInt();

            PlaceColor color = findColor(colorNum);

            PlaceTile tile = new PlaceTile(row, col, username, color);

            this.update(tile);
        }
        this.endGame();
    }

    /**
     * Launch the GUI.
     *
     * @param args not used, here, but named arguments are passed to the GUI.
     *             <code>--host=<i>hostname</i> --port=<i>portnum</i></code>
     */
    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Usage: java PlaceClient host port username");
            System.exit(0);
        }
        ConsoleApplication.launch(PlacePTUI.class, args);
    }

    /**
     * Unused method required by ConsoleApplication
     * @param o Not used
     * @param arg Not used
     */
    @Override
    public void update(Observable o, Object arg) {

    }
}
