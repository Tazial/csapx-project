package place.client.bots;

import javafx.application.Application;
import place.PlaceBoard;
import place.network.NetworkClient;

/**
 * An abstract class for the bots that makes sure the arguments are valid,
 * sets the variables for the connection and board, establishes the
 * connection, and handles errors. Contains an abstract method to be filled
 * in by the child bot classes with their main function.
 *
 * @Author Ari Wisenburn
 */
public abstract class ParentBot{
    //the hostname for the server
    private String hostname;
    //the port associated with the server
    private int port;
    //the username for the bot
    private String username;
    //a NetworkClient object representing a connection
    private NetworkClient conn;
    //a PlaceBoard object representing a canvas
    private PlaceBoard board;

    /**
     * The default ParentBot() constructor. If no constructors are passed in,
     * it sets the Strings to null and the port to 0.
     */
    public ParentBot(){
        hostname = null;
        port = 0;
        username = null;
    }

    /**
     * The ParentBot constructor. Takes in an array of strings and makes sure
     * the correct number of arguments are passed in and that they have the
     * correct types. Does not account for files specific bots may need, but
     * makes sure the bot at least has enough to make a connection to the
     * server. If the array does not have enough arguments or incorrect
     * types, an error message is displayed via the printUsage method and the
     * program quits. Otherwise, it sets the local variables to the arguments
     * passed in.
     * @param args a string of arguments taken in from the command line
     */
    public ParentBot(String[] args){
        //makes sure there are enough arguments to make a connection. If not,
        // it displays an error message via the printUsage method and quits.
        if (args.length < 3) {
            printUsage("Invalid number of arguments");
        }
        else {
            try {
                //assigns port to the integer value of args[1]
                port = Integer.parseInt(args[1]);
            }
            //if args[1] is not an integer, it displays an error message via
            // the printUsage method and quits.
            catch (NumberFormatException e) {
                printUsage("Port must be an integer");
            }
        }
        //assigns the username and hostname variables to their respective
        // arguments
        username = args[2];
        hostname = args[0];
        //creates a connection with the newly assigned variables
        connect(hostname, port, username);
        //assigns the board based on the connection that was just established
        board = conn.getBoard();
    }

    /**
     * a getter method for the username
     * @return a String representing the username
     */
    public String getUsername(){
        return username;
    }

    /**
     * a getter method for the hostname
     * @return a String representing the hostname
     */
    public String getHostname(){
        return hostname;
    }

    /**
     * a getter method for the port number
     * @return an int representing the port number
     */
    public int getPort(){
        return port;
    }

    /**
     * a getter method for the connection
     * @return a NetworkClient object representing the connection to the
     * server and a way to access the specific information within the server
     * (like the board)
     */
    public NetworkClient getConnection(){
        return conn;
    }

    /**
     * a getter method for the canvas
     * @return a PlaceBoard method representing the canvas associated with
     * the server
     */
    public PlaceBoard getCanvas(){
        return board;
    }

    /**
     * Prints an error associated with the arguments and then exits the program
     * @param error an error to be printed
     */
    protected void printUsage(String error) {
        System.out.println("Usage: java ParentBot <host> <port> <username>");
        System.out.println(error);
        System.exit(-1);
    }

    /**
     * A method that allows the bot to connect to the server with the given
     * variables
     * @param hostname a String representing the hostname of the server
     * @param port an int representing the port number for the server
     * @param username a String representing the bot's username to be seen by
     *                other users of the server
     */
    public void connect(String hostname, int port, String username){
        //tries to make a connection using the hostname, port, and username
        try {
            conn = new NetworkClient(hostname, port, username);
        }
        //catches and prints an interrupted exception
        catch (InterruptedException e) {
            System.err.println("Warning: Interrupted during login");
        }
        //catches and prints a general exception
        catch(Exception f){
            System.err.println(f);
        }
    }

    /**
     * an abstract method that all bots will have that performs the main
     * function of what the bots do.
     */
    abstract public void start();
}
