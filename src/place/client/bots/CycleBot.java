package place.client.bots;

import place.PlaceColor;

/**
 * A bot that cycles through every tile on the board, changing the color each time the cycle repeats
 *
 * @author Ari Wisenburg <aew1756@rit.edu>
 */
public class CycleBot extends ParentBot{
    /**
     * default constructor
     */
    public CycleBot(){
        super();
    }

    /**
     * constructor for CycleBot that calls the constructor of its parent
     * class, ParentBot
     * @param args an array of Strings passed in through the command line
     */
    public CycleBot(String[] args){
        //parent constructor
        super(args);
    }

    /**
     * The method that repeatedly
     * 1. generates a random color
     * 2. cycles through the board and changes every tile to that color once
     * 3. repeats
     */
    public void start(){
        while(true) {
            //get array of available colors
            PlaceColor[] availableColors = PlaceColor.values();
            //generate index of random color within the array
            int c = (int) (Math.random() * availableColors.length);
            //set color to the color at the randomly generated index
            PlaceColor color = availableColors[c];

            for(int row = 0; row < getCanvas().DIM; row++){
                for(int col = 0; col < getCanvas().DIM; col++){
                    //color the pixel
                    getConnection().changeTile(col, row, color);
                    //sleep
                    try{
                        Thread.sleep(500+100);
                    }
                    catch(InterruptedException e){
                        break;
                    }
                }
            }
        }
    }

    /**
     * The main method that creates a CycleBot object and runs the start
     * () method
     * @param args an array of Strings passed in through the command line
     */
    public static void main(String[] args) {
        CycleBot cycleBot = new CycleBot(args);
        cycleBot.start();
    }
}
