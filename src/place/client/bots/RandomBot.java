package place.client.bots;


import place.PlaceColor;

/**
 * This is a bot that connects to the server and, upon connection, begins
 * placing random color tiles on random pixels. It generates a random color
 * and a random point within the dimensions of the board, and then colors the
 * pixel at that point the color it chose.
 *
 * @author Ari Wisenburg <aew1756@rit.edu>
 */
public class RandomBot extends ParentBot{

    /**
     * default constructor
     */
    public RandomBot(){
        super();
    }

    /**
     * constructor for RandomBot that calls the constructor of its parent
     * class, ParentBot
     * @param args an array of Strings passed in through the command line
     */
    public RandomBot(String[] args){
        //parent constructor
        super(args);
    }

    /**
     * The method that repeatedly
     * 1. generates a random color
     * 2. generates a random coordinate within the dimensions of the board
     * 3. colors the point
     * 4. sleeps
     */
    public void start(){
        while(true) {
            //get array of available colors
            PlaceColor[] availableColors = PlaceColor.values();
            //generate index of random color within the array
            int c = (int) (Math.random() * availableColors.length);
            //set color to the color at the randomly generated index
            PlaceColor color = availableColors[c];

            //generate a random coordinate
            int y = (int) (Math.random() * (getCanvas().DIM));
            int x = (int) (Math.random() * (getCanvas().DIM));

            //color the pixel
            getConnection().changeTile(x, y, color);

            //sleep
            try{
                Thread.sleep(500+100);
            }
            catch(InterruptedException e){
                break;
            }
        }
    }

    /**
     * The main method that creates a RandomBot object and runs the start
     * () method
     * @param args an array of Strings passed in through the command line
     */
    public static void main(String[] args) {
        RandomBot randomBot = new RandomBot(args);
        randomBot.start();
    }
}
