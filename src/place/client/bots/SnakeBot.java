package place.client.bots;

import place.PlaceColor;

/**
 * A bot that snakes through every tile on the board before changing to a
 * random color
 *
 * @author Ari Wisenburg <aew1756@rit.edu>
 */
public class SnakeBot extends ParentBot{
    /**
     * default constructor
     */
    public SnakeBot(){
        super();
    }

    /**
     * constructor for SnakeBot that calls the constructor of its parent
     * class, ParentBot
     * @param args an array of Strings passed in through the command line
     */
    public SnakeBot(String[] args){
        //parent constructor
        super(args);
    }

    /**
     * The method that repeatedly
     * 1. generates a random color
     * 2. cycles through the board and changes every tile to that color once
     * 3. repeats
     */
    public void start(){
        while(true) {
            //get array of available colors
            PlaceColor[] availableColors = PlaceColor.values();
            //generate index of random color within the array
            int c = (int) (Math.random() * availableColors.length);
            //set color to the color at the randomly generated index
            PlaceColor color = availableColors[c];

            for(int row = 0; row < getCanvas().DIM; row++){
                //this colors the pixels going forward on rows 2n
                for(int col = 0; col < getCanvas().DIM; col++){
                    //color the pixel
                    getConnection().changeTile(col, row, color);
                    //sleep
                    try{
                        Thread.sleep(500+100);
                    }
                    catch(InterruptedException e){
                        break;
                    }
                }
                row++;
                //this colors the pixels going backward on rows 2n+1
                if(row < getCanvas().DIM) {
                    for (int col = getCanvas().DIM - 1; col >= 0; col--) {
                        //color the pixel
                        getConnection().changeTile(col, row, color);
                        //sleep
                        try {
                            Thread.sleep(500 + 100);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * The main method that creates a SnakeBot object and runs the start
     * () method
     * @param args an array of Strings passed in through the command line
     */
    public static void main(String[] args) {
        SnakeBot snakeBot = new SnakeBot(args);
        snakeBot.start();
    }
}

