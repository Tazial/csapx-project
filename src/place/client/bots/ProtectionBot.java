package place.client.bots;

import javafx.geometry.Point2D;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import place.PlaceColor;

import java.io.*;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import place.PlaceBoard;

/**
 * This class takes in a previously saved file for what to be protected and
 * loads it into the canvas. It protects that area that is loaded from the
 * file. It extends the class ParentBot and passes variables up to that class
 * for methods. It also handles potential errors associated with the file.
 *
 * @Author Ari Wisenburn
 */
public class ProtectionBot extends ParentBot{
    //a file to be read in
    private File file;

    /**
     * calls super constructor and checks to make sure the number of
     * arguments passed in is valid.
     * @param args a String array of arguments passed in through the command
     *             line
     */
    public ProtectionBot(String[] args){
        //parent constructor
        super(args);
        //checks if number of arguments is valid
        if (args.length < 4) {
            printUsage("Invalid number of arguments");
        }
        //sets local file to the file taken from the command line
        else {
            file = new File(args[3]);

        }
    }

    /**
     * Describes the usage of the class if an incorrect number of arguments
     * are passed in. Prints the error and exits the program
     * @param error a String describing the error that has occured.
     */
    @Override
    protected void printUsage(String error) {
        System.out.println("Usage: java ParentBot <host> <port> <username> " +
                "<fileName>");
        System.err.println(error);
        //exits program
        System.exit(-1);
    }

    /**
     * Checks to make sure the file is not null. If the file is not null, it
     * creates a new FileInputStream for the file and reads in the file. It
     * then protects the areas specified within the file and prints a message
     * confirming the connection was loaded successfully. Includes error
     * handling.
     */
    @Override
    public void start(){
        //makes sure file is not null
        if(file != null){
            //creates a new FileInputStream for the file to read in the
            // information. Calls a method loadProtections() associated with
            // the NetworkClient class to actually control which pixels are
            // protected.
            try {
                getConnection().loadProtections(new FileInputStream(file));
                //a printed message confirming the loadProtections() method
                // ran successfully.
                System.out.println("Info: Successfully loaded from " + getConnection());
            }
            //catches IO exception
            catch (IOException ex) {
                System.err.println(ex);
            }
            //catches Parse exception
            catch(ParseException ex) {
                System.err.println("Failed to read '" + file + "'");
            }
            //catches ArrayIndexOutOfBounds exception
            catch(ArrayIndexOutOfBoundsException ex) {
                System.err.println("The file '" + file + "' seems to have " +
                        "been made for a bigger board, and won't fit on this one.");
            }
        }
    }

    /**
     * The main method that creates a ProtectionBot object and runs the start
     * () method
     * @param args an array of Strings passed in through the command line
     */
    public static void main(String[] args) {
        ProtectionBot protect = new ProtectionBot(args);
        protect.start();
    }
}
