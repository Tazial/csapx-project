package place.client.gui;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.BoundingBox;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.net.ConnectException;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import place.PlaceBoard;
import place.PlaceColor;
import place.PlaceTile;
import place.network.NetworkClient;

/** A GUI Frontend to NetworkClient.  Adds means for the user to view a live
 * version of the canvas as well as to change and protect tiles as they see
 * fit.  Also adds an interface to save and restore configurations of protected
 * tiles to/from the disk.  For specifics on how to use the GUI, click on the
 * Instructions button from within the client.
 *
 * @author Taizo Simpson <tts1848@cs.rit.edu>
 * @author Ari Wisenburg <aew1756@rit.edu>
 */
public class PlaceGUI extends Application {

    // The Network Client being wrapped
    private NetworkClient conn;

    // A model of the board tracking the server's copy using this.conn
    private PlaceBoard board;

    // A reference to the GraphicsContext of the canvas in the GUI
    private GraphicsContext canvas;

    // A reference to the Canvas of the GUI
    private Canvas mainDisplay;

    // The color currently being placed by the user
    private PlaceColor color = PlaceColor.RED;

    // A reference to the info tooltip
    private Tooltip tooltip;

    // True after the client is connected to the server
    private boolean connected;

    // The error that occured while attempting to establish a connection / null
    private String connectError;

    // The width, in pixels, of each tile on the board
    private int scale = 25;

    @Override
    public void init() throws UnknownHostException, IOException {
        // Parse arguments
        List<String> args = getParameters().getRaw();
        String hostname = args.get(0);
        int port = Integer.parseInt(args.get(1));
        String username = args.get(2);

        // Establish connection and login
        try {
            this.conn = new NetworkClient(hostname, port, username);
        } catch (InterruptedException e) {
            System.err.println("Warning: Interrupted during login");
            return;
        } catch (ConnectException e) {
            System.err.println("Error: Connection refused");
            this.connectError = "Connection refused";
            return;
        } catch(SocketTimeoutException e) {
            System.err.println("Error: Connection timed out");
            this.connectError = "Connection timed out";
            return;
        } catch(UnknownHostException e) {
            System.err.println("Error: Unknown host");
            this.connectError = "Unknown host";
            return;
        }
        this.connected = true;
        this.conn.registerOnError(this::showWarning);
        this.board = this.conn.getBoard();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        if(!this.connected) {
            if(this.connectError != null) {
                this.showWarning(this.connectError);
            }
            return;
        }
        int dim = this.board.getBoard().length;
        Scene scene = new Scene(new HBox());
        this.mainDisplay = new Canvas(dim * this.scale, dim * this.scale);

        Pane tooltipGraphic = new Pane();
        tooltipGraphic.setMinHeight(this.scale / 2);
        tooltipGraphic.setMinWidth(this.scale / 2);

        this.tooltip = new Tooltip();
        this.tooltip.setGraphic(tooltipGraphic);

        HBox colorBox = new HBox();
        PlaceColor[] availableColors = PlaceColor.values();
        for(int i = 0; i < availableColors.length; i++) {
            final PlaceColor c = availableColors[i];
            Color color = mapColor(c);
            Pane colorPane = new Pane();
            colorPane.setBackground(
                new javafx.scene.layout.Background(
                    new javafx.scene.layout.BackgroundFill(
                        color,
                        new javafx.scene.layout.CornerRadii(0.0),
                        javafx.geometry.Insets.EMPTY
                    )
                )
            );
            colorPane.setMinHeight(20);
            colorBox.getChildren().add(colorPane);
            colorBox.setHgrow(colorPane, Priority.ALWAYS);
            colorPane.setOnMouseClicked((e) -> {
                this.color = c;
            });
        }
        colorBox.setMinHeight(this.scale);
        colorBox.setPadding(new Insets(5));

        this.canvas = this.mainDisplay.getGraphicsContext2D();
        this.conn.registerOnChange(this::update);
        this.redraw();

        // Register draw handlers
        BoundingBox bounds = new BoundingBox(0, 0, dim - 1, dim - 1);
        EventHandler<MouseEvent> drawHandler = (e) -> {
            int x = (int) Math.floor(e.getX() / this.scale);
            int y = (int) Math.floor(e.getY() / this.scale);
            Point2D point = new Point2D(x, y);
            Map<Point2D, PlaceColor> protections = this.conn.getProtections();
            if(bounds.contains(point)) {
                switch(e.getButton()) {
                    case PRIMARY:
                        if(protections.containsKey(point)) {
                            protections.remove(point);
                        }
                        this.conn.changeTile(x, y, this.color);
                        break;
                    case SECONDARY:
                        protections.put(point, this.color);
                }
                this.update(this.board.getTile(y, x));
            }
        };
        this.mainDisplay.setOnMouseClicked(drawHandler);
        this.mainDisplay.setOnMouseDragged(drawHandler);

        // Register tooltip handlers
        this.mainDisplay.setOnMouseMoved((e) -> {
            int x = (int) Math.floor(e.getX() / this.scale);
            int y = (int) Math.floor(e.getY() / this.scale);
            PlaceTile tile = this.board.getTile(y, x);
            long millis = tile.getTime();
            ZonedDateTime timestamp = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault());
            this.tooltip.setAnchorX(e.getScreenX() + 10);
            this.tooltip.setAnchorY(e.getScreenY() + 10);
            this.tooltip.setText(String.format(
                "(%d, %d)%n" +
                "%s%n" +
                "%tF%n" +
                "%<tT",
                x, y,
                tile.getOwner(),
                timestamp
            ));
            ((Pane) this.tooltip.getGraphic()).setBackground(
                new javafx.scene.layout.Background(
                    new javafx.scene.layout.BackgroundFill(
                        mapColor(tile.getColor()),
                        new javafx.scene.layout.CornerRadii(0.0),
                        javafx.geometry.Insets.EMPTY
                    )
                )
            );
        });
        this.mainDisplay.setOnMouseEntered((e) -> {
            this.tooltip.show(this.mainDisplay.getScene().getWindow());
        });
        this.mainDisplay.setOnMouseExited((e) -> {
            this.tooltip.hide();
        });

        ScrollPane scrollCanvas = new ScrollPane(this.mainDisplay);
        // Scroll when the appropriate key is pressed.
        scene.setOnKeyPressed((e) -> {
            double SCROLL_AMT = 0.05;
            double vChange = 0;
            double hChange = 0;
            int scaleChange = 0;
            switch(e.getCode()) {
                case W:
                case K:
                case UP:
                    vChange = -SCROLL_AMT;
                    break;
                case S:
                case J:
                case DOWN:
                    vChange = SCROLL_AMT;
                    break;
                case A:
                case H:
                case LEFT:
                    hChange = -SCROLL_AMT;
                    break;
                case D:
                case L:
                case RIGHT:
                    hChange = SCROLL_AMT;
                    break;
                case PLUS:
                case EQUALS:
                    scaleChange = 5;
                    break;
                case MINUS:
                    scaleChange = -5;
                    break;
            }
            if(vChange != 0) {
                scrollCanvas.setVvalue(
                    Math.max(
                        Math.min(
                            scrollCanvas.getVvalue() + vChange,
                            scrollCanvas.getVmax()
                        ),
                        scrollCanvas.getVmin()
                    )
                );
            }
            if(hChange != 0) {
                scrollCanvas.setHvalue(
                    Math.max(
                        Math.min(
                            scrollCanvas.getHvalue() + hChange,
                            scrollCanvas.getHmax()
                        ),
                        scrollCanvas.getHmin()
                    )
                );
            }
            if(scaleChange != 0) {
                this.resize(scaleChange);
            }
        });
        // Enable dragging using the middle mouse
        scrollCanvas.setOnMousePressed((e) -> {
            switch(e.getButton()) {
                case MIDDLE:
                    scrollCanvas.setPannable(true);
            }
        });
        scrollCanvas.setOnMouseReleased((e) -> {
            switch(e.getButton()) {
                case MIDDLE:
                    scrollCanvas.setPannable(false);
            }
        });
        // Hide scrollbars
        scrollCanvas.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollCanvas.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        Text saveInfo = new Text("Save and load the currently protected tiles into a file, so that you can exit the client without losing your configuration of protected tiles");
        saveInfo.setWrappingWidth(200);

        Button save = new Button("Save");
        save.setOnMouseClicked(e -> {
            FileChooser chooser = new FileChooser();
            File dest = chooser.showSaveDialog(primaryStage);
            if (dest != null) {
                try {
                    this.conn.saveProtections(new FileOutputStream(dest));
                    System.out.println("Info: Successfully saved to " + dest);
                } catch (IOException ex) {
                    this.showWarning(ex.toString());
                }
            }
        });

        Button load = new Button("Load");
        load.setOnMouseClicked(e -> {
            FileChooser chooser = new FileChooser();
            List<File> dests = chooser.showOpenMultipleDialog(primaryStage);
            for(File dest : dests) {
                try {
                    this.conn.loadProtections(new FileInputStream(dest))
                        .keySet()
                        .stream()
                        .map(point -> this.board.getTile((int) point.getY(), (int) point.getX()))
                        .forEach(this::update);

                    System.out.println("Info: Successfully loaded from " + dest);
                } catch (IOException ex) {
                    this.showWarning(ex.toString());
                } catch(ParseException ex) {
                    this.showWarning("Failed to read '" + dest + "'");
                } catch(ArrayIndexOutOfBoundsException ex) {
                    this.showWarning("The file '" + dest + "' seems to have been made for a bigger board, and won't fit on this one.");
                }
            }
        });

        /*
        Instructions button
         */
        Button instructions = new Button("Instructions");
        instructions.setOnMouseClicked(e -> {
            Stage secondStage = new Stage();
            secondStage.setTitle("INSTRUCTIONS");
            Text text = new Text(
                    "To change the color of a tile, select a color from the " +
                            "array of colors displayed. Then, click on the " +
                            "pixel you wish to change.\n\n" +
                            "To protect a tile, right click on it. This means" +
                            " that if the color of that tile is changed, the " +
                            "computer will change that back to the original " +
                            "color you selected at the next available time " +
                            "frame.\n\n" +
                            "To select multiple tiles at once, click and drag" +
                            " over the desired tiles. This works for both " +
                            "changing the color of the tiles and protecting " +
                            "tiles.\n\n" +
                            "To pan your view, you can use the arrow keys or " +
                            "WASD (up, left, down, and right, respectively)\n" +
                            "Alternatively, you can use HJKL, which " +
                            "would pan left, down, up, and right, in that " +
                            "order.");
            text.setWrappingWidth(500);
            BorderPane bordPane = new BorderPane();
            Button nextPage = new Button(">");
            Button previousPage = new Button("<");
            HBox bottomButton = new HBox(nextPage);
            nextPage.setOnMouseClicked(f -> {
                text.setText("To zoom in, press the \'+\' button. To zoom " +
                        "out, press the \'-\' button.\n\n" +
                        "The save button allows you to save your " +
                        "protected tiles from the input stream.\n\n" +
                        "The load button allows you to load protected tiles " +
                        "from the input stream.\n\n" +
                        "The clear button allows you to deselect all the " +
                        "blocks you have selected that haven't been colored " +
                        "in yet.");
                bordPane.setCenter(text);
                bordPane.setBottom(previousPage);
            });
            previousPage.setOnMouseClicked(g -> {
                text.setText("To change the color of a tile, select a color from the " +
                        "array of colors displayed. Then, click on the " +
                        "pixel you wish to change.\n\n" +
                        "To protect a tile, right click on it. This means" +
                        " that if the color of that tile is changed, the " +
                        "computer will change that back to the original " +
                        "color you selected at the next available time " +
                        "frame.\n\n" +
                        "To select multiple tiles at once, click and drag" +
                        " over the desired tiles. This works for both " +
                        "changing the color of the tiles and protecting " +
                        "tiles.\n\n" +
                        "To pan your view, you can use the arrow keys or " +
                        "WASD (up, left, down, and right, respectively)\n" +
                        "Alternatively, you can use HJKL, which " +
                        "would pan left, down, up, and right, in that " +
                        "order.");
                bordPane.setCenter(text);
                bordPane.setBottom(nextPage);
            });
            bordPane.setCenter(text);
            bordPane.setBottom(bottomButton);
            bordPane.setPadding(new Insets(25));
            secondStage.setScene(new Scene(bordPane));
            secondStage.show();
        });

        Button clear = new Button("Clear");
        clear.setOnMouseClicked(e -> {
            Collection<Point2D> protections = new HashSet<>(this.conn.getProtections().keySet());
            this.conn.getProtections().clear();
            protections.stream()
                       .map(point -> this.board.getTile((int) point.getY(), (int) point.getX()))
                       .forEach(this::update);
            System.out.println("Info: Protected tiles cleared");
        });

        HBox saveButtons = new HBox(10, save, load, clear);
        saveButtons.setAlignment(Pos.CENTER);

        HBox instructionButton = new HBox(10, instructions);
        instructionButton.setAlignment(Pos.CENTER);

        VBox controls = new VBox(8, saveInfo, saveButtons, instructionButton);
        controls.setPadding(new Insets(8));

        BorderPane bp = new BorderPane(scrollCanvas, null, controls, colorBox, null);
        scene.setRoot(bp);
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest((e) -> {
             this.conn.close();
        });
    }

    // Repaint all the tiles on the canvas
    private void redraw() {
        int dim = this.board.getBoard().length;
        for(int x = 0; x < dim; x++) {
            for(int y = 0; y < dim; y++) {
                PlaceTile tile = this.board.getTile(y, x);
                this.update(tile);
            }
        }
    }

    /**
     * Change the scale of the board by a certain amount.  Any change in scale
     * is added to the width of each pixel on the canvas.  So a change of +5
     * would make each tile 5 pixels wider and 5 pixels taller.  Note that this
     * will also trigger a redraw of the screen.
     *
     * @param scaleChange The amount to change the size of each pixel by.
     */
    public synchronized void resize(int scaleChange) {
        this.scale += scaleChange;
        int dim = this.board.getBoard().length;
        this.mainDisplay.setWidth(this.scale * dim);
        this.mainDisplay.setHeight(this.scale * dim);
        this.redraw();
    }

    /**
     * Redraw a specific tile.  This causes exactly one tile to be redrawn to
     * the color of the specified tile.  The tile itself isn't stored, so the
     * tooltip will still reflect the tile on the underlying board model, but
     * the color itself, as well as any annotations sugguesting that a tile is
     * in queue to replace this tile, or that the tile is protected, will be
     * updated.
     *
     * @param tile The tile to be redrawn
     */
    public synchronized void update(PlaceTile tile) {
        PlaceColor color = tile.getColor();
        int x = tile.getCol();
        int y = tile.getRow();
        Point2D location = new Point2D(x, y);
        PlaceColor protectedColor = this.conn.getProtections().get(location);

        if(protectedColor != null) {
            Color borderColor = mapColor(protectedColor);
            borderColor = protectedColor != PlaceColor.WHITE ? borderColor.brighter().desaturate() : borderColor.darker();
            this.canvas.setFill(borderColor);
            this.canvas.fillRect(x * this.scale, y * this.scale, this.scale, this.scale);
            this.canvas.setFill(mapColor(color));
            this.canvas.fillRect(((double) x + 0.1) * this.scale, ((double) y + 0.1) * this.scale, this.scale * 0.8, this.scale * 0.8);
        } else {
            this.canvas.setFill(mapColor(color));
            this.canvas.fillRect(x * this.scale, y * this.scale, this.scale, this.scale);
        }

        Map<Point2D, PlaceColor> queue = this.conn.getSendQueue();
        synchronized(queue) {
            queue = new HashMap<>(queue);
        }
        if(queue.containsKey(location)) {
            this.canvas.save();

            this.canvas.beginPath();
            this.canvas.rect(x * this.scale, y * this.scale, this.scale, this.scale);
            this.canvas.closePath();
            this.canvas.clip();

            this.canvas.setStroke(mapColor(queue.get(location)));
            this.canvas.setLineWidth(this.scale * 0.1);
            this.canvas.beginPath();
            this.canvas.moveTo(x * this.scale, (y + 1) * this.scale);
            this.canvas.lineTo((x + 1) * this.scale, y * this.scale);
            this.canvas.moveTo(x * this.scale, (y + 0.5) * this.scale);
            this.canvas.lineTo((x + 0.5) * this.scale, y * this.scale);
            this.canvas.moveTo((x + 0.5) * this.scale, (y + 1) * this.scale);
            this.canvas.lineTo((x + 1) * this.scale, (y + 0.5) * this.scale);
            this.canvas.closePath();
            this.canvas.stroke();

            this.canvas.restore();
        }
    }

    // Popup a warning dialog with the given error message
    private void showWarning(String error) {
        javafx.application.Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText(error);
            alert.show();
        });
    }

    // Convert a PlaceColor to the appropriate JavaFX color
    private static Color mapColor(PlaceColor color) {
        return new Color(color.getRed()/255.0, color.getGreen()/255.0, color.getBlue()/255.0, 1.0);
    }

    /**
     * Start up a GUI Client from the command line.  Three CL arguments are
     * accepted, all of which are mandatory.  The first argument should
     * indicate the hostname of the server, the second the port of the server,
     * and the third the username to connect under.  If any arguments are
     * incorrect, the GUI will not be started and an error will be displayed to
     * stdout.
     *
     * @param args The arguments passed via the command line
     */
    public static void main(String[] args) {
        if (args.length < 3) {
            printUsage("Invalid number of arguments");
        } else {
            try {
                Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                printUsage("Port must be an integer");
            }
            Application.launch(args);
        }
    }

    // Print a usage message and exit
    private static void printUsage(String error) {
        System.out.println("Usage: java PlaceGUI <host> <port> <username>");
        System.out.println(error);
        System.exit(-1);
    }
}
