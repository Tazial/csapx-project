package place.network;

import javafx.geometry.Point2D;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import place.PlaceBoard;
import place.PlaceColor;
import place.PlaceTile;

/** 
 * A high level interface for a remote Place server.  Handles communications
 * with the server and abstracts some of the protocol.  Uses a PlaceBoard as
 * the underlieing model and provides event listeners for events pushed from
 * the server.  Also handles queueing send requests, request deduplication, and
 * tile protection.
 *
 * Upon construction, the instance will run two threads, one responsible for
 * reading from the server, and one responsible for writing to it.  The read
 * thread simply waits for a message from the server and calls the approriate
 * handlers when it recieves one.
 *
 * Simultaniously, the write thread takes an action once every 500ms, the
 * standard time the server enforces between changing tiles.  When it's ready,
 * the thread will first check for any enqueued tiles.  If one is present and
 * is still valid (i.e. the tile being changed is a different color from the
 * color it is being changed to), that tile is sent to the server.  If there
 * are no valid enqueued tiles, the thread checks its set of protected tiles
 * for any tiles out of place.  If one is found, it will be corrected.  If
 * there are no valid enqueued tiles nor any out of place protected tiles, the
 * thread cannot take any action.
 *
 * @author Taizo Simpson <tts1848@cs.rit.edu>
 */
public class NetworkClient implements Closeable {

    // A socket wrapping the connection to the server
    private Socket sock;

    // Object streams to handle parsing messages from the server
    private ObjectInputStream in;
    private ObjectOutputStream out;

    // A local copy of the server's board
    private PlaceBoard board;

    // The username used by the client to communicate with the server
    private String username;

    // A map indicating tiles that should be protected by the client, as well
    // as the color they should be.
    private Map<Point2D, PlaceColor> protectedTiles = Collections.synchronizedMap(new HashMap<>());

    // An ordered map of tiles in queue to be changed.  This does not include
    // tiles that are changed because they are protected.
    private Map<Point2D, PlaceColor> sendQueue = Collections.synchronizedMap(new LinkedHashMap<>());

    // Threads the server keeps running
    private Thread readThread;
    private Thread sendThread;

    // Event handlers
    private Consumer<IOException> onDisconnect;
    private Consumer<PlaceTile> onChange;
    private Consumer<String> onError;

    /**
     * Creates a NetworkClient for the server on the given host and port.  In
     * the process, opens a socket to the server and starts two threads, one
     * for listening to the server, and one for sending updates to it.  These
     * remain open until they are interrupted, an error occurs, or the {@link
     * #close()} method is called.
     *
     * @param host The hostname or ip address of the server to connect to
     * @param port The port the server is listening on
     * @param username The username to use to communicate with the server
     * @throws UnknownHostException The hostname provided is invalid
     * @throws IOException A network error occured while connecting to the
     * @throws InterruptedException The constructor was interrupted while
     * handshaking with the server.
     * @throws ConnectException The connection was refused or otherwise failed
     */
    public NetworkClient(String host, int port, String username) throws UnknownHostException, IOException, InterruptedException, ConnectException, SocketTimeoutException {
        // Establish a connection to the server
        try {
            this.sock = new Socket();
            sock.connect(new InetSocketAddress(host, port), 1000);
            this.sock.setSoTimeout(1000);
            this.out = new ObjectOutputStream(this.sock.getOutputStream());
            this.in = new ObjectInputStream(this.sock.getInputStream());
            this.sock.setSoTimeout(0);
        } catch (IOException e) {
            if(this.sock != null) {
                this.sock.close();
            }
            throw e;
        }
        this.onError = msg -> System.out.println("Warning: Error received from server: " + msg);
        try {
            this.login(username);
        }
        catch(IOException e){
            System.err.println("Failed to log in");
            throw e;
        }

        // Setup default event handlers
        this.onDisconnect = (err) -> { synchronized(this) {
            try {
                this.sock.close();
                System.err.println("Info: Connection to server lost");
            } catch (IOException e) {}
            this.sendThread.interrupt();
            this.readThread.interrupt();
        }};

        this.onChange = (tile) -> { };



        // Start the threads
        this.readThread = new Thread(this::readLoop);
        this.readThread.start();
        this.sendThread = new Thread(this::sendLoop);
        this.sendThread.start();

        // Wait for handshake to complete
        synchronized(this) {
            while(this.board == null) {
                this.wait();
            }
        }
    }

    /** Closes the connection to the server and shuts down the running threads */
    public void close() {
        synchronized(this.sock) {
            try {
                this.sock.close();
            } catch (IOException e) {}
        }
    }

    // Starts the handshake with the server
    private synchronized void login(String username) throws IOException {
        synchronized(this.sock) {
            if(this.username == null) {
                this.out.writeUnshared(
                    new PlaceRequest<>(
                        PlaceRequest.RequestType.LOGIN,
                        username
                    )
                );
            }
        }
    }

    /**
     * Enqueue a tile to be changed at the next available opertunity.  The tile
     * will be last in queue, with one change being removed from the queue and
     * sent to the server every 500ms.  This is preferential to {@link
     * #sendTileRaw(int, int, PlaceColor)}, which does not respect the rate
     * limit set by the server, and can cause problems with ordering and
     * redundancy.  If a tile is enqueued that is already in the queue, the
     * previous color is overriden instead of adding the new color.
     *
     * @param x The x co-ordinate or column of the tile to change
     * @param y The y co-ordinate or row of the tile to change
     * @param color The color to change the tile to
     */
    public synchronized void changeTile(int x, int y, PlaceColor color) {
        this.sendQueue.put(new Point2D(x, y), color);
        this.notifyAll();
    }

    /**
     * Directly send a tile to the server.  This method corresponds on a lower
     * level to the CHANGE_TILE message of the protocol, although it does
     * implement a few other checks.  If the color provided is different from
     * the color of the tile at the given co-ordinates, the the appropriate
     * CHANGE_TILE message is sent to the server.  Note that for almost all
     * situations, the {@link #changeTile(int, int, PlaceColor} method is
     * perfered because it supports queueing and the server's rate limits.
     *
     * @param x The x co-ordinate or column of the tile to change
     * @param y The y co-ordinate or row of the tile to change
     * @param color The color to change the tile to
     * @throws IOException There was a network error while communicating with
     * the server.
     */
    public boolean sendTileRaw(int x, int y, PlaceColor color) throws IOException {
        PlaceTile modifiedTile = this.board.getTile(y, x);
        if(modifiedTile.getColor() != color) {
            modifiedTile = new PlaceTile(
                modifiedTile.getRow(),
                modifiedTile.getCol(),
                this.username,
                color,
                System.currentTimeMillis()
            );
            synchronized(this.out) {
                this.out.writeUnshared(
                    new PlaceRequest<>(
                        PlaceRequest.RequestType.CHANGE_TILE,
                        modifiedTile
                    )
                );
            }
            System.out.printf("Info: Sent tile [%d, %d] as %s%n", x, y, color);
            return true; // Success
        } else {
            return false; // No need, tile already correct
        }
    }

    /**
     * Write the currently protected tiles to an output stream, where another
     * NetworkClient can restore them.  This could be used to transfer tile
     * protections between two different clients, or to prevent losing data
     * when the client closes.  The format the data is stored in is, for each
     * tile:
     *
     *    4 bytes for the x position
     *    4 bytes for the y position
     *    1 byte for the color
     *
     * @param dest The OutputStream into which to write the data
     * @throws IOException There was an error writing the data to the stream
     */
    public void saveProtections(OutputStream dest) throws IOException {
        HashMap<Point2D, PlaceColor> protectedTiles;
        synchronized(this.protectedTiles) {
            protectedTiles = new HashMap<>(this.protectedTiles);
        }
        ByteBuffer out = ByteBuffer.allocate(9 * protectedTiles.size());
        for(Map.Entry<Point2D, PlaceColor> prot : protectedTiles.entrySet()) {
            out.putInt((int) prot.getKey().getX());
            out.putInt((int) prot.getKey().getY());
            out.put((byte) prot.getValue().getNumber());
        }
        dest.write(out.array());
        dest.flush();
    }

    /**
     * Load protected tiles from an input stream.  Uses the same format as
     * {@link saveProtections(OutputStream)}.  Will not trigger any events, so
     * redrawing clients might not happen.  Make sure you do this manually.
     *
     * @param from The InputStream to load the tiles from
     * @return A Map containing all changed tile protections
     * @throws IOException There was an error reading the data in the stream
     * @throws ArrayIndexOutOfBoundsException The stream references a tile that
     *                                        doesn't exist on this board, but
     *                                        might exist on a bigger board.
     * @throws ParseException Invalid data is encountered while parsing the
     *                        stream.
     */
    public Map<Point2D, PlaceColor> loadProtections(InputStream from) throws IOException, ArrayIndexOutOfBoundsException, ParseException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        byte[] buffer = new byte[9];
        while(from.available() > 0) {
            from.read(buffer);
            bytes.write(buffer);
        }

        ByteBuffer in = ByteBuffer.wrap(bytes.toByteArray());
        HashMap<Point2D, PlaceColor> changes = new HashMap<>(in.remaining() / 9);
        int boardSize = this.board.getBoard().length;
        while(in.remaining() >= 9) {
            int x = in.getInt();
            int y = in.getInt();
            byte color_indx = in.get();
            if(Math.max(x, y) > boardSize) {
                throw new ArrayIndexOutOfBoundsException("The provided input stream contains tiles outside the board");
            } else if(Math.min(Math.min(x, y), color_indx) < 0 || color_indx >= PlaceColor.TOTAL_COLORS) {
                throw new ParseException("Invalid data found in stream", in.position() - 9);
            }
            changes.put(
                new Point2D(
                    x,
                    y
                ),
                PlaceColor.values()[color_indx]
            );
        }

        this.protectedTiles.putAll(changes);
        return changes;
    }

    /**
     * Register a listener for the client disconnecting.  The provided consumer
     * will be called with the IOException that first indicated the disconnect.
     * This is called regardless of who initiated the disconnect.  Any open
     * resources will be closed for you.
     *
     * @param onDisconnect The consumer to take the exception when the client
     *                     disconnects.
     */
    public void registerOnDisconnect(Consumer<IOException> onDisconnect) {
        synchronized(this.onDisconnect) {
            this.onDisconnect = this.onDisconnect.andThen(onDisconnect);
        }
    }

    /**
     * Register an event listener for when a tile is receieved from the server.
     * Ths consumer will be given the specific tile that changed, and the board
     * will have been updated to match.  This should probably be used to update
     * the user's display.
     *
     * @param onChange The consumer to call on a tile change
     */

    public void registerOnChange(Consumer<PlaceTile> onChange) {
        synchronized(this.onChange) {
            this.onChange = this.onChange.andThen(onChange);
        }
    }

    /**
     * Register an event listener for the when an error is received by the
     * sever.  This is NOT called when a local error occurs, specifically when
     * the server sends an ERROR message.  The provided consumer will be given
     * the message sent by the server along with the ERROR.
     *
     * @param onError The consumer to be called with the error message
     */
    public void registerOnError(Consumer<String> onError) {
        synchronized(this.onError) {
            this.onError = this.onError.andThen(onError);
        }
    }

    /**
     * Get a map indicating which tiles are under the protection of the
     * NetworkClient.  Updating the map WILL update, and is the sugguested way
     * to update, the tiles actually being protected by the NetworkClient.
     * Simultaniously, any updates to the tiles protected by the client will be
     * reflected in the returned map.
     *
     * The map itself is corresponds a point to a color.  The point, i.e. the
     * map's key, represents the co-ordinates of the protected tile, and the
     * value is the color that the NetworkClient should attempt to keep the
     * tile as.
     *
     * @return The client's map of protected tiles.
     */
    public Map<Point2D, PlaceColor> getProtections() {
        return this.protectedTiles;
    }

    /**
     * Get a map indicating the tiles in queue to be placed.  This is an
     * ordered map indicating all the tiles yet to be changed by the client.
     * The tiles are sent in FIFO order, see {@link LinkedHashMap} for more
     * details.  While you can add points to the send queue using this method,
     * it's preferred that the {@link changeTile(PlaceTile)} method be used instead.
     *
     * Similar to {@link getProtections()}, the map returned by this method
     * corresponds the location of a tile to the color that it is to be changed
     * to.
     *
     * @return The queued tiles to be changed
     */
    public Map<Point2D, PlaceColor> getSendQueue() {
        return this.sendQueue;
    }

    /**
     * Get the model of the board.  This returns the board originally sent by
     * the server to the client, updated with any changes that have occurred
     * since then.  Future changes will be reflected in the returned board.
     *
     * @return The internal model of the board
     */
    public PlaceBoard getBoard() {
        return this.board;
    }

    /**
     * Get the username that was accepted by the server.  This is exactly the
     * username sent by the server in the LOGIN_SUCCESS message.
     *
     * @return The username sent to the client.
     */
    public String getUsername() {
        return  this.username;
    }

    // The loop responsible for reading message from the server
    private void readLoop() {
        synchronized(this.in) {
            while(true) {
                try {
                    PlaceRequest<?> msg = (PlaceRequest<?>) this.in.readUnshared();
                    switch(msg.getType()) {
                        case LOGIN_SUCCESS: // The username sent by the client was accepted by the server
                            this.username = (String) msg.getData();
                            System.err.println("Info: Login accepted with username \"" + username + "\"");
                            break;
                        case BOARD: // The server is telling us the board that is being used
                            synchronized(this) {
                                this.board = (PlaceBoard) msg.getData();
                                this.notifyAll(); // The constructor will wait until the board is available, so we should wake it up
                            }
                            break;
                        case TILE_CHANGED: // Someone changed a tile
                            PlaceTile tile = (PlaceTile) msg.getData();
                            synchronized(this) {
                                this.board.setTile(tile);
                                this.onChange.accept(tile);
                                this.notifyAll();
                            }
                            break;
                        case ERROR:
                            synchronized(this) {
                                this.onError.accept((String) msg.getData());
                            }
                            break;
                        default:
                            System.err.println("Warning: Unrecognized message type from server: " + msg.getType());
                    }
                } catch(ClassNotFoundException e) {
                    System.err.println("Warning: Failed to parse message from server");
                } catch(IOException e) {
                    synchronized(this) {
                        this.onDisconnect.accept(e);
                    }
                    break;
                }
            }
        }
    }

    // The loop responsible for sending tiles to the server
    private void sendLoop() {
        while(true) {
            try {
                synchronized(this) {
                    while(!sendSingle()) {
                        this.wait();
                    }
                }
                Thread.sleep(500 + 100);
            } catch (InterruptedException e) {
                break;
            } catch (IOException e) {
                System.err.println("Warning: Failed to send tile to server.");
            }
        }
    }

    // Identify what tile should be sent to the server, and send it.
    private synchronized boolean sendSingle() throws IOException {
        // Try to send a manually queued tile
        synchronized(this.sendQueue) {
            Iterator<Map.Entry<Point2D, PlaceColor>> it = this.sendQueue.entrySet().iterator();
            while(it.hasNext()) {
                Map.Entry<Point2D, PlaceColor> ent = it.next();
                it.remove();
                Point2D loc = ent.getKey();
                if(this.sendTileRaw((int) loc.getX(), (int) loc.getY(), ent.getValue())) {
                    return true;
                }
            }
        }

        // Repair a protected tile
        List<Map.Entry<Point2D, PlaceColor>> shuffledProtections = new ArrayList<>(this.protectedTiles.entrySet());
        Collections.shuffle(shuffledProtections);
        for(Map.Entry<Point2D, PlaceColor> entry : shuffledProtections) {
            Point2D point = entry.getKey();
            int x = (int) point.getX();
            int y = (int) point.getY();
            PlaceTile tile = this.board.getTile(y, x);
            if(this.sendTileRaw(x, y, entry.getValue())) {
                return true;
            }
        }

        return false;
    }

}
