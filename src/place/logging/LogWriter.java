package place.logging;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import place.PlaceTile;

/**
 * Represents a writable log file.  Each entry in the log file is in the format
 * of:
 *
 *    2 bytes representing a signed short for the tile row
 *    2 bytes, signed short, tile col
 *    1 byte, PlaceColor, tile color
 *    8 bytes, signed long, millisecond of event
 *    2 bytes, signed short, number of bytes in user's name
 *    n bytes, String, user's name
 *
 * Entries are not seperated by any delimiter.
 *
 * @author Taizo Simpson <tts1848@cs.rit.edu>
 */
public class LogWriter {

    private DataOutputStream output;

    /**
     * Construct a new LogWriter around the given stream.
     *
     * @param logTo The stream to log the changes to
     */
    public LogWriter(OutputStream logTo) throws IOException, SecurityException {
        this.output = new DataOutputStream(logTo);
    }

    /**
     * Appends an entry to the end of the file.  It is assumed that the
     * timestamp on the tile is later than any timestamps already logged.
     *
     * @param tile The tile that was just changed, and should be recorded.
     * @throws IOExcpetion An I/O Error occured
     */
    public synchronized void writeEntry(PlaceTile tile) throws IOException {
        this.output.writeShort(tile.getRow());
        this.output.writeShort(tile.getCol());
        this.output.writeByte((byte) tile.getColor().getNumber());
        this.output.writeLong(tile.getTime());
        this.output.writeUTF(tile.getOwner());
        this.output.flush();
    }
}
